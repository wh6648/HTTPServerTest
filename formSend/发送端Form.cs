﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace formSend
{
    public partial class 发送端Form : Form
    {
        public 发送端Form()
        {
            InitializeComponent();
            RichTextBox.CheckForIllegalCrossThreadCalls = false;
        }

        private void 发送Btn_Click(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(DoPost), null);
        }

        private void DoPost(object obj)
        {
            string postData = null;
            string rtn = null;
            try
            {
                postData = string.Empty;
                rtn = string.Empty;

                Dictionary<string, string> dic = (Dictionary<string, string>)obj;

                string url = "http://127.0.0.1:52587/MonitorTransaction/test01.json";
                postData = JsonConvert.SerializeObject(new { sendTime = DateTime.Now.Ticks });
                rtn = PostUrl(url, postData);

                发送内容rtb.Clear();
                发送内容rtb.AppendText(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\t" + rtn + "\n");

                //让文本框获取焦点
                发送内容rtb.Focus();
                //设置光标的位置到文本尾
                发送内容rtb.Select(发送内容rtb.TextLength, 0);
                //滚动到控件光标处
                发送内容rtb.ScrollToCaret();
            }
            catch (Exception e)
            {
                发送内容rtb.AppendText(e.Message + "\n");
            }
        }

        private string PostUrl(string url, string postData)
        {
            string result = "";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            req.Method = "POST";

            req.Timeout = 30 * 1000;//设置请求超时时间，单位为毫秒

            req.ContentType = "application/json";

            byte[] data = Encoding.UTF8.GetBytes(postData.Replace("\r\n", "\\n").Replace("\r", "\\n").Replace("\n", "\\n"));

            req.ContentLength = data.Length;

            using (Stream reqStream = req.GetRequestStream())
            {
                reqStream.Write(data, 0, data.Length);

                reqStream.Close();
            }

            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            Stream stream = resp.GetResponseStream();

            //获取响应内容
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                result = reader.ReadToEnd();
            }

            return result;
        }
    }
}
