﻿namespace formSend
{
    partial class 发送端Form
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.发送Btn = new System.Windows.Forms.Button();
            this.发送内容rtb = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // 发送Btn
            // 
            this.发送Btn.Location = new System.Drawing.Point(12, 12);
            this.发送Btn.Name = "发送Btn";
            this.发送Btn.Size = new System.Drawing.Size(88, 61);
            this.发送Btn.TabIndex = 0;
            this.发送Btn.Text = "发送";
            this.发送Btn.UseVisualStyleBackColor = true;
            this.发送Btn.Click += new System.EventHandler(this.发送Btn_Click);
            // 
            // 发送内容rtb
            // 
            this.发送内容rtb.Location = new System.Drawing.Point(106, 12);
            this.发送内容rtb.Name = "发送内容rtb";
            this.发送内容rtb.ReadOnly = true;
            this.发送内容rtb.Size = new System.Drawing.Size(471, 61);
            this.发送内容rtb.TabIndex = 1;
            this.发送内容rtb.Text = "";
            // 
            // 发送端Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 83);
            this.Controls.Add(this.发送内容rtb);
            this.Controls.Add(this.发送Btn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "发送端Form";
            this.Text = "发送端";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button 发送Btn;
        private System.Windows.Forms.RichTextBox 发送内容rtb;
    }
}

