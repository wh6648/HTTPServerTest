﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace formReceive
{
    public partial class 接收端Form : Form
    {
        private static HttpListener httpPostRequest = new HttpListener();

        public 接收端Form()
        {
            InitializeComponent();
            RichTextBox.CheckForIllegalCrossThreadCalls = false;

            httpPostRequest.Prefixes.Add("http://127.0.0.1:52587/MonitorTransaction/");
            httpPostRequest.Start();

            Thread ThrednHttpPostRequest = new Thread(new ThreadStart(HttpPostRequestHandle));
            ThrednHttpPostRequest.Start();
        }

        public string ShowRequestData(HttpListenerRequest request)
        {
            if (!request.HasEntityBody)
            {
                Console.WriteLine("No client data was sent with the request.");
                return "";
            }
            System.IO.Stream body = request.InputStream;
            System.Text.Encoding encoding = request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            if (request.ContentType != null)
            {
                Console.WriteLine("Client data content type {0}", request.ContentType);
            }
            Console.WriteLine("Client data content length {0}", request.ContentLength64);

            Console.WriteLine("Start of client data:");
            // Convert the data to a string and display it on the console.
            string s = reader.ReadToEnd();
            Console.WriteLine(s);
            Console.WriteLine("End of client data:");
            body.Close();
            reader.Close();
            // If you are finished with the request, it should be closed also.
            return s;
        }

        private void HttpPostRequestHandle()
        {
            while (true)
            {
                HttpListenerContext requestContext = httpPostRequest.GetContext();
                Thread threadsub = new Thread(new ParameterizedThreadStart((requestcontext) =>
                {
                    HttpListenerContext request = (HttpListenerContext)requestcontext;

                    //获取Post请求中的参数和值帮助类  
                    HttpListenerPostParaHelper httppost = new HttpListenerPostParaHelper(request);
                    //获取Post过来的参数和数据  
                    string strRequest = ShowRequestData(request.Request);
                    long sendTime = 0;
                    if (!string.IsNullOrEmpty(strRequest))
                    {
                        JObject json = (JObject)JsonConvert.DeserializeObject(strRequest);
                        sendTime = Convert.ToInt64(json["sendTime"]);

                        接收内容rtb.Clear();
                        接收内容rtb.AppendText(new DateTime(sendTime).ToString("yyyy-MM-dd HH:mm:ss") + "\n");
                    }

                    //Response  
                    request.Response.StatusCode = 200;
                    request.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                    request.Response.ContentType = "application/json";
                    requestContext.Response.ContentEncoding = Encoding.UTF8;
                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new { success = "true", msg = "提交成功" }));
                    request.Response.ContentLength64 = buffer.Length;
                    Stream output = request.Response.OutputStream;
                    output.Write(buffer, 0, buffer.Length);
                    output.Close();
                }));
                threadsub.Start(requestContext);
            }
        }
    }
}
